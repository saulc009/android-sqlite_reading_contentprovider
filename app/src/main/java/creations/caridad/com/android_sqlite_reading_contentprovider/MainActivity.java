package creations.caridad.com.android_sqlite_reading_contentprovider;

import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import creations.caridad.com.android_sqlite_reading_contentprovider.provider.ProviderDataContract;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // call methods here
        setData();
    }

    private void setData() {
        // retrieve our data in other app DB
        Uri uri = Uri.parse(ProviderDataContract.DATASOURCEURI);
        Cursor cursor = getContentResolver().query(uri, ProviderDataContract.COLUMNS, null, null, null);

        // find our data from other app DB
        MyCustomAdapter adapter = new MyCustomAdapter(this, cursor);

        // XML UI
        ListView listView = (ListView) findViewById(R.id.mainListView);
        listView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.buttonRefresh:
                setData();
                break;
            default:
                return false;
        }
        return true;
    }
}