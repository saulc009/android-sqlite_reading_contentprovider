package creations.caridad.com.android_sqlite_reading_contentprovider.provider;

/**
 * Created by Saul on 6/11/17.
 */

public class ProviderDataContract {

    // apps can access our db with this uri
    public static final String CONTENTURI = "content://com.android_sqlite_contentprovider/";

    public static final String TABLENAME = "users";

    public static final String DATASOURCEURI = CONTENTURI + TABLENAME;

    // Table Properties
    public static final String FirstName = "firstname";
    public static final String LastName = "lastname";
    public static final String Age = "age";
    public static final String ID = "_id";

    // returns all columns
    public static final String[] COLUMNS = {
            ID, FirstName, LastName, Age
    };
}

