package creations.caridad.com.android_sqlite_reading_contentprovider;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.widget.ResourceCursorAdapter;
import android.widget.TextView;

import creations.caridad.com.android_sqlite_reading_contentprovider.provider.ProviderDataContract;

/**
 * Created by Saul on 6/11/17.
 */

public class MyCustomAdapter extends ResourceCursorAdapter {

    public MyCustomAdapter(Context context, Cursor c) {
        super(context, R.layout.custom_adapter_row, c, 0);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        // set XML ui
        // set user text
        TextView textViewFirstName = (TextView) view.findViewById(R.id.textUserName);
        textViewFirstName.setText(cursor.getString(cursor.getColumnIndex(ProviderDataContract.FirstName)));

        TextView textViewLastName = (TextView) view.findViewById(R.id.textUserAge);
        textViewLastName.setText(cursor.getString(cursor.getColumnIndex(ProviderDataContract.Age)));
    }
}