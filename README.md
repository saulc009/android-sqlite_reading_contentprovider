# README #

This app reads the ContentProvider Database from another android app located in the repositories, link will be located below. The app reads the user name and displays them in a list from the other android app which is exporting there databse. This app uses the permissions to access the other apps Uri. 

### This app ONLY works with Android_SQLite_ContentProvider ###
* [SQLite_ContentProvider](https://bitbucket.org/saulc009/android-sqlite_contentprovider)

### Official Documentation ###

* [Uri](https://developer.android.com/reference/android/net/Uri.html)

* [ResourceCursorAdapter](https://developer.android.com/reference/android/widget/ResourceCursorAdapter.html)